# ReadPoKaifu

Our RPK app will allow users to find book selections by interest, as well as discuss collections with other users. RPK will allow users to use advanced filters and criteria to accurately determine the desired collection, also relying on the opinions.


So what is the GitHub workflow?

1. The content of the master branch is always deployable.
 
2. When starting work on something new, fork off the master branch with a name that matches its purpose (for example, "new-oauth2-scopes").
 
3. Having committed to this branch locally, regularly submit your work to the same-named branch on the server.
 
4. When you need feedback, or help, or when you find the branch ready to be merged, submit a pull request.
 
5. After someone else has reviewed and approved the feature, you can merge your branch into the master branch.
 
6. After the master branch has been updated with new code, you can immediately push it into production and you should do so.